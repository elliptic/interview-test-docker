const express = require('express');
const http = require('http');

module.exports = function createServer() {
  const web = express();
  web.enable('trust proxy');
  web.use('/ping', (req, res, next) => res.json({ message: 'pong!' }));

  return http.createServer(web);
};
