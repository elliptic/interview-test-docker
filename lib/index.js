/* eslint-disable no-console */
const createServer = require('./web.js');

const app = createServer();
const port = process.env.PORT;
app.listen(port);
console.log(`app listening on port ${port}`);
