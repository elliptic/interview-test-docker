const chai = require('chai');
const createServer = require('../lib/web');
const supertest = require('supertest');

chai.should();

describe('ping', () => {
  it('gets pong', async () => {
    const app = createServer();
    const res = await supertest(app).get('/ping').expect(200);
    res.body.message.should.equal('pong!');
  });
});
