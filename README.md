# Simple Web App

### Quick Start

Create a '.env' file using the template example and fill with relevant values.
Then

```bash
nvm use
npm install
npm start
```

### Tests

Run `npm run test:all`
